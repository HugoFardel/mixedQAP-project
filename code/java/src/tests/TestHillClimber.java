/**
 *
 */
import algo.HillClimber;
import algo.RandomSearch;
import evaluation.FullNeighborhoodEval;
import evaluation.FullNeighborhoodIncrementalEval;
import evaluation.MixedQAPeval;
import evaluation.Solution;

import java.io.*;
import java.util.Random;

//import evaluation.Solution;

/**
 * @author verel
 *

Author:
Sebastien Verel,
Univ. du Littoral Côte d'Opale, France.
version 0 : 2020/11/24

 */
public class TestHillClimber {

    /**
     * @param args
     */
    public static void main(String[] args) throws FileNotFoundException {
        String instanceFileName = "instances/mixedQAP_uni_20_1_100_1.dat";
        int n = 20;
        MixedQAPeval eval = new MixedQAPeval(instanceFileName);

        System.out.println(" Eval : " + eval);

        Solution solution = new Solution(n);

        for(int i = 0; i < n; i++) {
            solution.xlist.add(1.0/n);
            solution.sigmalist.add(i);
        }
        //0 1 2 14 4 3 6 7 18 9 10 11 12 17 5 15 16 13 8 19 en normal
        //0 1 15 3 5 4 6 7 18 9 10 19 12 17 14 2 16 8 13 11 en incremental
        eval.apply(solution);
        System.out.println("solution avant hillClimber : " + solution);
        HillClimber hillClimber = new HillClimber(eval,new FullNeighborhoodIncrementalEval(eval),n);
        hillClimber.run(solution);
        eval.apply(solution); // selon evaluation normal ou incrmentale il faut le mettre
        System.out.println("fitness de la solution " + solution.fitness);

        System.out.println("Solution : " + solution);
        System.out.println();
     // solution.printOnFlow();
    }


}
