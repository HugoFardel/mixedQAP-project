import algo.ES;
import algo.ILS;
import evaluation.FullNeighborhoodEval;
import evaluation.MixedQAPeval;
import evaluation.Solution;

import java.io.FileNotFoundException;

public class testES {

    public static void main(String[] args) throws FileNotFoundException {
        String instanceFileName = "instances/mixedQAP_uni_20_1_100_1.dat";
        int n = 20;
        MixedQAPeval eval = new MixedQAPeval(instanceFileName);

        Solution solution = new Solution(n);

        for(int i = 0; i < n; i++) {
            solution.xlist.add(1.0/n);
            solution.sigmalist.add(i);
        }

        eval.apply(solution);
        System.out.println("solution avant ES : " + solution);
        ES es = new ES(eval,n);
        es.run(solution);
        System.out.println("fitness de la solution " + solution.fitness);

        System.out.println("Solution : " + solution);
        //solution.printOnFlow();
    }
}
