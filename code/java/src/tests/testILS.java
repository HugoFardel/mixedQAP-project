
import algo.ILS;
import evaluation.FullNeighborhoodEval;
import evaluation.MixedQAPeval;
import evaluation.Solution;

import java.io.FileNotFoundException;

public class testILS {

    public static void main(String[] args) throws FileNotFoundException {
        String instanceFileName = "instances/mixedQAP_uni_20_1_100_1.dat";
        int n = 20;
        MixedQAPeval eval = new MixedQAPeval(instanceFileName);

        System.out.println(" Eval : " + eval);

        Solution solution = new Solution(n);

        for(int i = 0; i < n; i++) {
            solution.xlist.add(1.0/n);
            solution.sigmalist.add(i);
        }

        eval.apply(solution);
        System.out.println(" solution avant ILS : " + solution);
        ILS ils = new ILS(eval,new FullNeighborhoodEval(eval),n);
        ils.run(solution);
        System.out.println("fitness de la solution " + solution.fitness);

        System.out.println("Solution : " + solution);
        System.out.println();
        //solution.printOnFlow();
    }
}
