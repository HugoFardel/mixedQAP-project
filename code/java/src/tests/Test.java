/**
 * 
 */
import algo.RandomSearch;
import evaluation.MixedQAPeval;
import evaluation.Solution;

import java.io.*;
import java.util.Random;

/**
 * @author verel
 *

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
  version 0 : 2020/11/24

 */
public class Test {

    /**
     * @param args
     */
    public static void main(String[] args) throws FileNotFoundException {
    	String instanceFileName = "instances/mixedQAP_uni_20_1_100_1.dat";
		int n=20;
    	MixedQAPeval eval = new MixedQAPeval(instanceFileName);

	   	System.out.println(" Eval : " + eval);

	    Solution solution = new Solution(n);

	    for(int i = 0; i < n; i++) {
    	    solution.xlist.add(1.0/n);
        	solution.sigmalist.add(i);
    	}

	 	eval.apply(solution);
		RandomSearch randomSearch = new RandomSearch(new Random(),eval);
	    randomSearch.run(solution);
		System.out.println("fitness de la solution " + solution.fitness);

    	System.out.println("Solution : " + solution);
      	System.out.println();
    	solution.printOnFlow();
    }


}
