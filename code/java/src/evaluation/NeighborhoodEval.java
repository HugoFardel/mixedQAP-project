package evaluation;


public abstract class NeighborhoodEval {

    public NeighborhoodEval(){

    }

    public abstract void init(Solution s, double[][] delta);

    public abstract void update(Solution s, double[][] delta);

}
