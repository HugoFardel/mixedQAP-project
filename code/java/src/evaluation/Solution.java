package evaluation;

import java.util.ArrayList;
import java.util.List;

/**

   Classe pour représenter une solution à variable mixte:
     vector of double and vector of permutation
     vector de la permutation

*/
public class Solution {
    // vector of real numbers
    public List<Double> xlist;

    // permutation
    public List<Integer> sigmalist;

    // fitness value 
    public double fitness;

    // true when the vector x have been modified
    public boolean modifiedX;

    // personnal flow matrix when x is constant
    public double [] [] flow;

    /***********************************
     * Constructor
     ***********************************/
    public Solution() {
        this.fitness   = 0.0;
        this.modifiedX = false;
        this.flow      = null;
        this.xlist = new ArrayList<>();
        this.sigmalist = new ArrayList<Integer>();
    }

    public Solution(int n) {
        this.flow  = new double[n][n];

        this.modifiedX = true;
        this.fitness   = 0.0;
        this.xlist = new ArrayList<>();
        this.sigmalist = new ArrayList<>();
    }

    public Solution(Solution _s) {
        this.xlist = new ArrayList<>();
        this.sigmalist = new ArrayList<>();
        this.flow = null;
        if (!_s.xlist.isEmpty()) {
            this.flow  = new double[_s.flow.length][_s.flow.length];

            for(int i = 0; i < _s.xlist.size(); i++) {
                this.xlist.add(i,_s.xlist.get(i));
                this.sigmalist.add(i,_s.sigmalist.get(i));
                this.flow[i]  = new double[_s.flow[i].length];// PEUT ETRE A VIRER

                for(int j = 0; j < _s.flow.length; j++) {
                    this.flow[i][j] = _s.flow[i][j];
                }
            }
        } else {
            this.xlist = new ArrayList<>();
            this.sigmalist = new ArrayList<>();
            this.flow = null;
        }

        this.modifiedX = _s.modifiedX;
        this.fitness = _s.fitness;
    }

    public Solution clone() {
        Solution solution = new Solution(this);
        return solution;
    }

    public void copy(Solution _s) {
        this.xlist = new ArrayList<>();
        this.sigmalist = new ArrayList<>();
        this.flow = null;
        if (!_s.xlist.isEmpty()) {
            this.flow = new double[_s.flow.length][_s.flow.length];

            for (int i = 0; i < _s.xlist.size(); i++) {
                this.xlist.add(i, _s.xlist.get(i));
                this.sigmalist.add(i, _s.sigmalist.get(i));

                for (int j = 0; j < _s.flow.length; j++) {
                    this.flow[i][j] = _s.flow[i][j];
                }
            }

        }
        this.modifiedX = _s.modifiedX;
        this.fitness = _s.fitness;

    }



    /**
     * set the fitness
     */
    public void fitness(double _fit) {
        this.fitness = _fit;
    }

    /**
     * print the solution
     */
    public String toString() {
        String s = this.fitness + " " ;

        if (this.xlist.isEmpty()) {
            s += "" ;
        } else {
            s += this.xlist.size();

            for(int i = 0; i < this.xlist.size(); i++) {
                s += " " + xlist.get(i);
            }

            for(int i = 0; i < this.sigmalist.size(); i++) {
                s += " " + sigmalist.get(i);
            }
        }
        return s;
    }

    /**
     * print the flow matrix
     */
    public void printOnFlow() {
        System.out.println(modifiedX);
        for(int i = 0; i < this.sigmalist.size(); i++) {

            for(int j = 0; j < this.sigmalist.size(); j++)
                System.out.print(" " + flow[i][j]) ;
            System.out.println();
        }
    }
    public void setX(int index, double value){
        this.xlist.set(index,value);
    }
}

