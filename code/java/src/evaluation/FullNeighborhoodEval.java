package evaluation;


public class FullNeighborhoodEval extends NeighborhoodEval {

    protected MixedQAPeval eval;

    public FullNeighborhoodEval(MixedQAPeval eval) {
        this.eval = eval;
    }

    @Override
    public void init(Solution s, double[][] delta) {
        int tmp,i,j;
        double currentFitness = s.fitness;

        for(i=1;i<s.sigmalist.size();i++){
            for (j=0;j<i;j++){
                tmp = s.sigmalist.get(i);
                s.sigmalist.set(i,s.sigmalist.get(j));
                s.sigmalist.set(j,tmp);

                this.eval.apply(s);
                delta[i][j] = s.fitness - currentFitness;
                s.sigmalist.set(j,s.sigmalist.get(i));
                s.sigmalist.set(i,tmp);
            }
        }
        s.fitness = currentFitness;
    }

    @Override
    public void update(Solution s, double[][] delta) {
        init(s,delta);
    }
}
