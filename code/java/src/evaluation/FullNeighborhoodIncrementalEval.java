package evaluation;

public class FullNeighborhoodIncrementalEval extends NeighborhoodEval {
    protected MixedQAPeval eval;

    public FullNeighborhoodIncrementalEval(MixedQAPeval eval) {
        this.eval = eval;
    }

    @Override
    public void init(Solution s, double[][] delta) {
        int tmp,i,j;
        double currentFitness = s.fitness;

        for(i=1;i<s.sigmalist.size();i++){
            for (j=0;j<i;j++){
                s.fitness = currentFitness;
                this.eval.incrementalApply(s,i,j);
                delta[i][j] = s.fitness - currentFitness;
            }
        }
        s.fitness = currentFitness;
    }

    @Override
    public void update(Solution s, double[][] delta) {
        init(s,delta);
    }
}
