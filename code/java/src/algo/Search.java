package algo;

import evaluation.Solution;


public abstract class Search {

    long time = System.currentTimeMillis();

    public abstract void run(Solution s);

    public abstract void timeLimit(long time_limite);
}
