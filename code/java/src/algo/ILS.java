package algo;

import evaluation.MixedQAPeval;
import evaluation.NeighborhoodEval;
import evaluation.Solution;

import java.util.Random;

public class ILS extends Search{
    int first=0, second=0;
    public MixedQAPeval eval;
    public NeighborhoodEval neighborhoodEval;
    public int n;
    public double[][] delta;

    public ILS(MixedQAPeval eval, NeighborhoodEval neighborhoodEval, int n){
        this.eval = eval;
        this.neighborhoodEval = neighborhoodEval;
        this.n = n;
        this.delta = new double[n][n];
        timeLimit(5000);

    }

    @Override
    public void run(Solution s) {
        Solution s2 = s.clone();

        localSearch(s);
        while(System.currentTimeMillis() < this.time){
            s2 = s.clone();
            perturbation(s2,5);
            localSearch(s2);
            if(s2.fitness < s.fitness){
                s.copy(s2);
            }
        }
    }
    public void perturbation(Solution s,int k){
        Random r = new Random();
        int alea1=0;
        int alea2=0;
        for (int i=0;i<k;i++){
            do{
                 alea1=r.nextInt(s.xlist.size());
                 alea2=r.nextInt(s.xlist.size());
            }while (alea1==alea2);
            int tmp = s.sigmalist.get(s.sigmalist.indexOf(alea1));
            s.sigmalist.set(s.sigmalist.indexOf(alea1),s.sigmalist.get(s.sigmalist.indexOf(alea2)));
            s.sigmalist.set(s.sigmalist.indexOf(alea2),tmp);
        }
        eval.apply(s);
    }
    public void localSearch(Solution s){
        neighborhoodEval.init(s,delta);
        selectBestNeighbor();
        if(delta[first][second] < 0){
            int tmp = s.sigmalist.get(s.sigmalist.indexOf(first));
            s.sigmalist.set(s.sigmalist.indexOf(first),s.sigmalist.get(s.sigmalist.indexOf(second)));
            s.sigmalist.set(s.sigmalist.indexOf(second),tmp);
            s.fitness += delta[first][second];

        }
    }
    public void selectBestNeighbor(){
        int i,j;

        this.first = 1;
        this.second = 0;

        double bestDelta = delta[first][second];
        for(i=1; i<n; i++){
            for(j=0; j<i; j++){
                if(delta[i][j] < bestDelta){
                    this.first = i;
                    this.second = j;
                    bestDelta = delta[i][j];
                }
            }
        }
    }

    @Override
    public void timeLimit(long time_limite) {
        this.time = System.currentTimeMillis()+time_limite;
    }
}

