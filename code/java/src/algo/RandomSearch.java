package algo;

import evaluation.MixedQAPeval;
import evaluation.Solution;
import operator.RandomPermutation;
import operator.UniformContinue;

import java.util.Random;

public class RandomSearch extends Search{

    protected Random rng;
    protected MixedQAPeval eval;

    public RandomSearch(Random rng, MixedQAPeval eval) {
        this.rng = rng;
        this.eval = eval;
        timeLimit(1000);
    }

    @Override
    public void run(Solution solution) {
        RandomPermutation randomPermutation = new RandomPermutation(rng,solution.sigmalist.size());
        UniformContinue uniform = new UniformContinue (solution.xlist.size());

        Solution s =  solution.clone();
        uniform.run(s);
        while (System.currentTimeMillis() < this.time){
            randomPermutation.run(s);
            this.eval.apply(s);
            if (s.fitness < solution.fitness) {
                solution.copy(s);
            }
        }
    }

    @Override
    public void timeLimit(long time_limite) {
        this.time = System.currentTimeMillis()+time_limite;
    }
}
