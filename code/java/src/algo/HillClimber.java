package algo;

import evaluation.MixedQAPeval;
import evaluation.NeighborhoodEval;
import evaluation.Solution;

public class HillClimber extends Search {

    int first=0, second=0;
    public MixedQAPeval eval;
    public NeighborhoodEval neighborhoodEval;
    public int n;
    public double[][] delta;

    public HillClimber(MixedQAPeval eval, NeighborhoodEval neighborhoodEval, int n){
        this.eval = eval;
        this.neighborhoodEval = neighborhoodEval;
        this.n = n;
        this.delta = new double[n][n];
        timeLimit(1000);

    }

    @Override
    public void run(Solution s) {
        boolean localOpt = false;
        int compteurTemps=0;
        neighborhoodEval.init(s,delta);

        while((!localOpt && System.currentTimeMillis() < this.time) || compteurTemps<500 ){
            compteurTemps++;
            selectBestNeighbor();
            if(delta[first][second] < 0){
                int tmp = s.sigmalist.get(s.sigmalist.indexOf(first));
                s.sigmalist.set(s.sigmalist.indexOf(first),s.sigmalist.get(s.sigmalist.indexOf(second)));
                s.sigmalist.set(s.sigmalist.indexOf(second),tmp);

                s.fitness += delta[first][second];
                neighborhoodEval.update(s,delta);
            }
            else{
                localOpt = true;
            }

        }
    }

    public void selectBestNeighbor(){
        int i,j;

        this.first = 1;
        this.second = 0;

        double bestDelta = delta[first][second];
        for(i=1; i<n; i++){
            for(j=0; j<i; j++){
                if(delta[i][j] < bestDelta){
                    this.first = i;
                    this.second = j;
                    bestDelta = delta[i][j];
                }
            }
        }
    }

    @Override
    public void timeLimit(long time_limite) {
        this.time = System.currentTimeMillis()+time_limite;
    }
}
