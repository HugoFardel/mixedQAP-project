package algo;

import evaluation.MixedQAPeval;
import evaluation.Solution;


public class ES extends Search{

    public MixedQAPeval eval;
    public int n;
    public double sigma = 0.9;
    public int gamma = 2;

    public ES(MixedQAPeval eval, int n){
        this.eval = eval;
        this.n = n;

    }

    private void majXList(Solution solution){
        double sommeX=0;
        for (int i=0;i<solution.xlist.size();i++){
            solution.setX(i,solution.xlist.get(i)*sigma * (Math.random()*2 -1));
            if (solution.xlist.get(i) < 0) {
                solution.setX(i,-solution.xlist.get(i));
            }
            sommeX += solution.xlist.get(i);
        }
        if (sommeX ==0){
            for (int i=0;i<solution.xlist.size();i++) solution.setX(i,1/((double)this.n));
        }
        else if (sommeX !=1) for (int i=0;i<solution.xlist.size();i++)  solution.setX(i,solution.xlist.get(i)/ sommeX);

    }

    @Override
    public void run(Solution s) {
        int iteration=1;
        Solution clone = null;
        double lastFitness=0;
        while (iteration <10000) {
            clone = s.clone();
            majXList(clone);
            clone.modifiedX=true;
            eval.apply(clone);
            if (iteration%200==0) lastFitness=s.fitness;
            if (clone.fitness < s.fitness){
                s.copy(clone);
                sigma*=gamma;
            }else sigma*= (Math.pow(gamma,-0.25));
            if (s.fitness-lastFitness < 0.00001) iteration=9999;
            iteration++;
        }
    }

    @Override
    public void timeLimit(long time_limite) {

    }


}
