package operator;

import evaluation.Solution;

import java.util.Collections;
import java.util.Random;

public class RandomPermutation extends Operator{

    protected Random rng;
    protected int tailleSigma;

    public RandomPermutation(Random rng, int tailleSigma) {
        this.rng = rng;
        this.tailleSigma = tailleSigma;
    }


    @Override
    public void run(Solution s) {
        if(tailleSigma < s.sigmalist.size())
            s.sigmalist = s.sigmalist.subList(0,tailleSigma);

        Collections.shuffle(s.sigmalist, rng);
    }
}
