package operator;

import evaluation.Solution;

public abstract class Operator {

    public abstract void run(Solution s);
}
