package operator;

import evaluation.Solution;


public class UniformContinue extends Operator{

    protected int n;

    public UniformContinue(int n) {
        this.n = n;
    }

    @Override
    public void run(Solution s) {
        if(n < s.xlist.size()) {
            s.xlist = s.xlist.subList(0, n);
        }
        for (int j=0;j<n;j++) {
            s.xlist.set(j,1.0/n);
        }


        s.modifiedX = true;
    }
}
