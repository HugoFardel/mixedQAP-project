#ifndef _RandomSearch_h
#define _RandomSearch_h

#include <random>

#include <solution.h>
#include <mixedQAPeval.h>
#include <search.h>
#include <randomPermutation.h>
#include <uniformContinue.h>

/******************************************************
 * Random Search
 * 
 *
 */
class RandomSearch : public Search {
public:
  /** constructor
   *
   * @param _eval evaluation function
   * @param 
   */
  RandomSearch(std::default_random_engine & _rng, Eval & _eval) : rng(_rng), eval(_eval) { 
  }

  // the search
  virtual void operator()(Solution & _solution) {
    RandomPermutation random(rng, _solution.sigma.size());
    UniformContinue uniform(_solution.x.size());

    Solution s(_solution.sigma.size());
    uniform(s);

    while(time(NULL) < timeLimit_) {
      random(s);

      eval(s);

      if (s.fitness < _solution.fitness)
        _solution = s;
    }
  }

protected:
  // pseudo-random generator 
  std::default_random_engine & rng;

  // evaluation function
  Eval & eval;   

};


#endif
